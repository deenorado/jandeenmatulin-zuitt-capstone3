# Shopping cart

A simple fake store React app.

## Built with

- [React](https://reactjs.org/)
- [React Router](https://reactrouter.com/)
- [styled-components](https://styled-components.com/)
