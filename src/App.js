import React, { useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Header from "./components/Header";
import Main from "./components/Main";
import Footer from "./components/Footer";
import Cart from "./components/Cart";
import { UserContext } from "./userContext";

const App = () => {
  const [cart, setCart] = useState([]);
  const [isCartOpen, setIsCartOpen] = useState();
  const [user, setUser] = useState({
    accessToken: localStorage.getItem("accessToken"),
    isAdmin: localStorage.getItem("isAdmin") === "true",
  });
  const [loggedIn, setLoggedIn] = useState(false);

  const handleCartClose = () => {
    setIsCartOpen(false);
  };

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      accessToken: null,
      isAdmin: null,
    });
    setLoggedIn(false);
  };

  return (
    <React.Fragment>
      <UserContext.Provider
        value={{
          user,
          setUser,
          unsetUser,
          loggedIn,
          setLoggedIn,
          cart,
          setCart,
          isCartOpen,
          setIsCartOpen,
          handleCartClose,
        }}
      >
        <Router>
          <Header />
          <Main />
          <Footer />
        </Router>
        <Cart />
      </UserContext.Provider>
    </React.Fragment>
  );
};

export default App;
