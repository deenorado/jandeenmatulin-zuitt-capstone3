import React, { useContext } from "react";
import styled from "styled-components";
import { Link, useHistory } from "react-router-dom";
import { FaShoppingCart } from "react-icons/fa";
import Button from "./elements/Button";
import { UserContext } from "../userContext";

const Header = () => {
  const { unsetUser, setIsCartOpen } = useContext(UserContext);

  const history = useHistory();

  const handleLogout = () => {
    unsetUser();
    localStorage.clear();
    history.push("/");
  };
  return (
    <HeaderWrapper>
      <Container>
        <Link to={"/"}>
          <Logo>Switcheroo</Logo>
        </Link>
        <Navbar>
          <Link to={"/"}>
            <ButtonContainer>
              <Button
                content="Home"
                color="dark"
                animation="color"
                shape="round"
              ></Button>
            </ButtonContainer>
          </Link>
          <Link to={"/products"}>
            <ButtonContainer>
              <Button
                content="Products"
                color="dark"
                animation="color"
                shape="round"
              ></Button>
            </ButtonContainer>
          </Link>
          {localStorage.getItem("isLoggedIn") === "true" &&
            localStorage.getItem("isAdmin") === "true" && (
              <Link to={"/orders"}>
                <ButtonContainer>
                  <Button
                    content="Orders"
                    color="dark"
                    animation="color"
                    shape="round"
                  ></Button>
                </ButtonContainer>
              </Link>
            )}
          {localStorage.getItem("isLoggedIn") === "true" &&
            localStorage.getItem("isAdmin") === "null" && (
              <Link to={"/profile"}>
                <ButtonContainer>
                  <Button
                    content="Profile"
                    color="dark"
                    animation="color"
                    shape="round"
                  ></Button>
                </ButtonContainer>
              </Link>
            )}
          {localStorage.getItem("isLoggedIn") === "true" ? (
            <ButtonContainer>
              <Button
                content="Logout"
                color="dark"
                animation="color"
                shape="round"
                onClick={handleLogout}
              ></Button>
            </ButtonContainer>
          ) : (
            <Link to={"/login"}>
              <ButtonContainer>
                <Button
                  content="Log In or Register"
                  color="dark"
                  animation="color"
                  shape="round"
                ></Button>
              </ButtonContainer>
            </Link>
          )}
          {localStorage.getItem("isAdmin") === "null" &&
            localStorage.getItem("isLoggedIn") === "true" && (
              <ButtonContainer onClick={() => setIsCartOpen(true)}>
                <Button
                  content={<FaShoppingCart />}
                  shape="round"
                  animation="scale"
                />
              </ButtonContainer>
            )}
        </Navbar>
      </Container>
    </HeaderWrapper>
  );
};

const HeaderWrapper = styled.header`
  background-color: ${({ theme }) => theme.colors.dark};
`;

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  max-width: ${({ theme }) => theme.widths.content};
  margin: 0 auto;
  padding: 4rem;

  @media (max-width: 768px) {
    flex-direction: column;
    gap: 4rem;
  }
`;

const Logo = styled.h1`
  color: ${({ theme }) => theme.colors.light};
  font-size: 6rem;
`;

const Navbar = styled.nav`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 7rem;
  font-size: 2.4rem;

  @media (max-width: 480px) {
    gap: 0;
    width: 100%;
  }
`;

const ButtonContainer = styled.div`
  position: relative;
  cursor: pointer;
  transition: transform 0.15s ease-in-out;

  &:hover {
    transform: scale(1.1);
  }

  &:active {
    transform: scale(1.02);
  }
`;

export default Header;
