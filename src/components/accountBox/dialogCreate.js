import React, { useContext, useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import { BoxContainer, FormContainer, SubmitButton } from "./common";
import {
  TextField,
  OutlinedInput,
  CircularProgress,
  Fab,
} from "@material-ui/core";
import { Marginer } from "../marginer";
import { UserContext } from "../../userContext";
import styled from "styled-components";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function AlertDialogSlide() {
  const [open, setOpen] = React.useState(false);
  const [productName, setProductName] = useState("");
  const [productDescription, setProductDescription] = useState("");
  const [productPrice, setProductPrice] = useState("");
  const [loading, setLoading] = useState(false);

  const { user } = useContext(UserContext);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleNameChange = (e) => {
    setProductName(e.target.value);
  };

  const handleDescriptionChange = (e) => {
    setProductDescription(e.target.value);
  };

  const handlePriceChange = (e) => {
    setProductPrice(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);

    fetch("https://switch-online-store.onrender.com/admin/create-product", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.accessToken}`,
      },
      body: JSON.stringify({
        name: productName,
        description: productDescription,
        price: productPrice,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        setLoading(false);
        window.location.reload();
      });
  };

  return (
    <div>
      <Fab
        children="+"
        color="secondary"
        size="large"
        style={{
          fontSize: "50px",
          position: "fixed",
          top: "80%",
          left: "75%",
          height: "70px",
          width: "70px",
        }}
        onClick={handleClickOpen}
      />

      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        fullWidth={true}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <BoxContainer>
          <HeaderContainer>
            <HeaderText>Add a product</HeaderText>
          </HeaderContainer>
          <Marginer direction="vertical" margin="1.6em" />
          <FormContainer id="product-name">
            <OutlinedInput
              name="productName"
              placeholder="Name of product"
              value={productName}
              onChange={(e) => handleNameChange(e)}
              style={{ fontSize: "1.6rem" }}
            />
            <Marginer direction="vertical" margin="1.6em" />
            <TextField
              name="productDescription"
              placeholder="Description"
              value={productDescription}
              onChange={(e) => handleDescriptionChange(e)}
              variant="outlined"
              size="medium"
              multiline={true}
              inputProps={{
                style: { fontSize: 16, lineHeight: 1.2 },
              }}
            />
            <Marginer direction="vertical" margin="1.6em" />
            <OutlinedInput
              type="number"
              name="productPrice"
              value={productPrice}
              placeholder="Price"
              onChange={(e) => handlePriceChange(e)}
              style={{ fontSize: "1.6rem" }}
            />
          </FormContainer>
          <Marginer direction="vertical" margin="1.6em" />
          {loading ? (
            <CircularProgress color="secondary" />
          ) : (
            <SubmitButton
              type="submit"
              onClick={(e) => handleSubmit(e)}
              form="login-form"
            >
              Create
            </SubmitButton>
          )}
          <Marginer direction="vertical" margin="2em" />
        </BoxContainer>
      </Dialog>
    </div>
  );
}

const HeaderContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const HeaderText = styled.h2`
  font-size: 40px;
  font-weight: 600;
  line-height: 1.24;
  color: #696969;
  z-index: 0;
  margin: 0;
`;
