import React, { useState } from "react";
import styled from "styled-components";
import { LoginForm } from "./loginForm";
import { motion } from "framer-motion";
import { AccountContext } from "./accountContext";
import { SignupForm } from "./signupForm";
import { AdminLoginForm } from "./adminLoginForm";

const BoxContainer = styled.div`
  width: 100%;
  min-height: 100%;
  display: flex;
  flex-direction: column;
  border-radius: 19px;
  background-color: #fff;
  position: relative;
  overflow: hidden;
`;

const TopContainer = styled.div`
  width: 100%;
  height: 200px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  padding: 0 1.8em;
  position: relative;
  padding-bottom: 5em;
`;

const BackDrop = styled(motion.div)`
  width: 10%;
  height: 10px;
  position: absolute;
  display: flex;
  flex-direction: column;
  border-radius: 50%;
  transform: rotate(60deg);
  top: -500px;
  left: 450px;
  background-color: #46FFD3;
  z-index: 10;
  );
`;

const HeaderContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const HeaderText = styled.h2`
  font-size: 40px;
  font-weight: 600;
  line-height: 1.24;
  color: #696969;
  z-index: 0;
  margin: 0;
`;

const SmallText = styled.h5`
  color: #696969;
  font-weight: 500;
  font-size: 20px;
  z-index: 0;
  margin: 0;
  margin-top: 7px;
`;

const InnerContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding: 0 1.8em;
`;

const backdropVariants = {
  expanded: {
    width: "233%",
    height: "1500px",
    borderRadius: "20%",
    transform: "translateX(-1000px)",
  },
  collapsed: {
    width: "800%",
    height: "550px",
    borderRadius: "50%",
    transform: "translateX(1000px)",
  },
};

const expandingTransition = {
  type: "spring",
  duration: 2.3,
  stiffness: 30,
};

export function AccountBox() {
  const [isExpanded, setExpanded] = useState(false);
  const [active, setActive] = useState("signin");

  const playExpandingAnimation = () => {
    setExpanded(true);
    setTimeout(() => {
      setExpanded(false);
    }, expandingTransition.duration * 1000 - 1500);
  };

  const switchToSignup = () => {
    playExpandingAnimation();
    setTimeout(() => {
      setActive("signup");
    }, 1000);
  };

  const switchToSignin = () => {
    playExpandingAnimation();
    setTimeout(() => {
      setActive("signin");
    }, 1000);
  };

  const switchToAdminSignin = () => {
    playExpandingAnimation();
    setTimeout(() => {
      setActive("adminsignin");
    }, 1000);
  };

  const contextValue = { switchToSignup, switchToSignin, switchToAdminSignin };

  return (
    <AccountContext.Provider value={contextValue}>
      <BoxContainer>
        <BackDrop
          initial={false}
          animate={isExpanded ? "expanded" : "collapsed"}
          variants={backdropVariants}
          transition={expandingTransition}
        />
        <TopContainer>
          {active === "signin" && (
            <HeaderContainer>
              <HeaderText>Welcome Back</HeaderText>
              <SmallText>Please sign in to continue!</SmallText>
            </HeaderContainer>
          )}
          {active === "signup" && (
            <HeaderContainer>
              <HeaderText>Create Account</HeaderText>
              <SmallText>Please sign up to continue!</SmallText>
            </HeaderContainer>
          )}
          {active === "adminsignin" && (
            <HeaderContainer>
              <HeaderText>Login as Admin</HeaderText>
              <SmallText>Please sign in to continue!</SmallText>
            </HeaderContainer>
          )}
        </TopContainer>
        <InnerContainer>
          {active === "signin" && <LoginForm />}
          {active === "signup" && <SignupForm />}
          {active === "adminsignin" && <AdminLoginForm />}
        </InnerContainer>
      </BoxContainer>
    </AccountContext.Provider>
  );
}
