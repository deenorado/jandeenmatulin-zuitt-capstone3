import React, { useContext, useState } from "react";
import {
  BoldLink,
  BoxContainer,
  FormContainer,
  Input,
  MutedLink,
  SubmitButton,
} from "./common";
import { Marginer } from "../marginer";
import { AccountContext } from "./accountContext";
import { CircularProgress } from "@material-ui/core";
import { useSnackbar } from "notistack";

export function SignupForm() {
  const { switchToSignin, switchToAdminSignin } = useContext(AccountContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [verifyPassword, setVerifyPassword] = useState("");
  const [fullName, setFullName] = useState("");
  const [about, setAbout] = useState("");
  const [loading, setLoading] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const handleChange = (e) => {
    switch (e.target.name) {
      case "email":
        setEmail(e.target.value);
        break;
      case "password":
        setPassword(e.target.value);
        break;
      case "verifyPassword":
        setVerifyPassword(e.target.value);
        break;
      case "fullName":
        setFullName(e.target.value);
        break;
      case "about":
        setAbout(e.target.value);
        break;
      default:
        break;
    }
  };

  const handleRegister = (e) => {
    e.preventDefault();
    setLoading(true);

    fetch("https://switch-online-store.onrender.com/users/signup", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
        fullName,
        about,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        enqueueSnackbar("Signed up successfully, please log in", {
          variant: "success",
        });
        setLoading(false);
        switchToSignin();
      });
  };

  return (
    <BoxContainer>
      <FormContainer id="submit-form">
        <Input
          type="text"
          placeholder="Full Name"
          name="fullName"
          value={fullName}
          onChange={(e) => handleChange(e)}
        />
        <Input
          type="text"
          placeholder="About Me"
          name="about"
          value={about}
          onChange={(e) => handleChange(e)}
        />
        <Input
          type="email"
          placeholder="Email"
          name="email"
          value={email}
          onChange={(e) => handleChange(e)}
        />
        <Input
          type="password"
          placeholder="Password"
          name="password"
          value={password}
          onChange={(e) => handleChange(e)}
        />
        <Input
          type="password"
          placeholder="Confirm Password"
          name="verifyPassword"
          value={verifyPassword}
          onChange={(e) => handleChange(e)}
        />
      </FormContainer>
      <Marginer direction="vertical" margin="1.6em" />
      {loading ? (
        <CircularProgress color="secondary" />
      ) : (
        <SubmitButton
          type="submit"
          form="submit-form"
          onClick={(e) => handleRegister(e)}
        >
          Sign Up
        </SubmitButton>
      )}
      <Marginer direction="vertical" margin="2em" />
      <span>
        <MutedLink href="#" onClick={switchToSignin}>
          Already have an account?
        </MutedLink>
        <BoldLink href="#" onClick={switchToSignin}>
          Sign in
        </BoldLink>
      </span>
      <BoldLink href="#" onClick={switchToAdminSignin}>
        Or sign in as an admin
      </BoldLink>
      <Marginer direction="vertical" margin="1em" />
    </BoxContainer>
  );
}
