import React, { useContext, useState } from "react";
import styled from "styled-components";
import Button from "../elements/Button";
import { Marginer } from "../marginer";
import { UserContext } from "../../userContext";
import {
  CircularProgress,
  Dialog,
  OutlinedInput,
  Slide,
} from "@material-ui/core";
import { BoxContainer, FormContainer, SubmitButton } from "./common";
import { useHistory } from "react-router-dom";
import { useSnackbar } from "notistack";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const ProfileCard = ({ email, id, fullName, about }) => {
  const [open, setOpen] = useState(false);
  const [password, setPassword] = useState("");
  const [verifyPassword, setVerifyPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const { setIsCartOpen, unsetUser } = useContext(UserContext);
  const { enqueueSnackbar } = useSnackbar();

  const history = useHistory();

  const handleDialogOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleVerifyPasswordChange = (e) => {
    setVerifyPassword(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);

    fetch(
      "https://switch-online-store.onrender.com/users/change-password",
      {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          password: password,
        }),
      }
    )
      .then((res) => res.json())
      .then((result) => {
        if (result.error) {
          setLoading(false);
          enqueueSnackbar("Failed to change password, please try again", {
            variant: "error",
          });
          history.go(0);
        }
        setLoading(false);
        enqueueSnackbar("Successfully changed password, please log in again", {
          variant: "success",
        });
        unsetUser();
        history.push("/login");
      });
  };

  return (
    <CardContainer>
      <HeaderImage>
        <ProfileImage
          src={
            "https://images.creativemarket.com/0.1.0/ps/8562057/580/387/m2/fpnw/wm0/cover-.jpg?1592494694&s=85b85c36949b1ba1ceda05798dc73594"
          }
          alt="Gamer logo"
        />
      </HeaderImage>
      <BoldText>{fullName}</BoldText>
      <SubHeader>{email}</SubHeader>
      <RegularText>{about}</RegularText>
      <Container>
        <Button
          content="View Orders"
          size="wide"
          shape="round"
          color="primary"
          animation="color"
          onClick={() => setIsCartOpen(true)}
        ></Button>
        <Marginer direction="vertical" margin="0.5em" />
        <Button
          content="Change Password"
          size="wide"
          shape="round"
          color="red"
          animation="color"
          onClick={handleDialogOpen}
        ></Button>
        <Dialog
          open={open}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleClose}
          fullWidth={true}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <BoxContainer>
            <HeaderContainer>
              <HeaderText>Update your password</HeaderText>
            </HeaderContainer>
            <Marginer direction="vertical" margin="1.6em" />
            <FormContainer id="product-name">
              <OutlinedInput
                name="password"
                placeholder="New password"
                value={password}
                onChange={(e) => handlePasswordChange(e)}
                style={{ fontSize: "1.6rem" }}
                type="password"
              />
              <Marginer direction="vertical" margin="1.6em" />
              <OutlinedInput
                name="verifyPassword"
                placeholder="Verify new password"
                value={verifyPassword}
                onChange={(e) => handleVerifyPasswordChange(e)}
                style={{ fontSize: "1.6rem" }}
                type="password"
              />
            </FormContainer>
            <Marginer direction="vertical" margin="1.6em" />
            {loading ? (
              <CircularProgress color="secondary" />
            ) : (
              <SubmitButton
                type="submit"
                onClick={(e) => handleSubmit(e)}
                form="login-form"
              >
                Update
              </SubmitButton>
            )}
            <Marginer direction="vertical" margin="2em" />
          </BoxContainer>
        </Dialog>
      </Container>
    </CardContainer>
  );
};

const CardContainer = styled.div`
  background-color: white;
  min-width: 80%;
  height: 100%;
  margin: auto;
  border-radius: 32px;
  box-shadow: 0px 10px 30px hsl(185, 75%, 35%);
`;

const HeaderImage = styled.header`
  background: url("https://as1.ftcdn.net/v2/jpg/03/23/88/08/1000_F_323880864_TPsH5ropjEBo1ViILJmcFHJqsBzorxUB.jpg");
  background-repeat: no-repeat;
  background-size: auto;
  text-align: center;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
`;

const ProfileImage = styled.img`
  margin: auto;
  width: 200px;
  height: 200px;
  border: solid white 4px;
  border-radius: 50%;
`;

const BoldText = styled.h1`
  font-weight: bold;
  font-size: 4rem;
  text-align: center;
  padding: 10px 20px 0px 20px;
`;

const SubHeader = styled.h2`
  font-weight: normal;
  font-size: 2rem;
  color: hsl(0, 0%, 50%);
  text-align: center;
  padding-bottom: 10px;
`;

const RegularText = styled.p`
  font-weight: normal;
  font-size: 2rem;
  text-align: center;
  padding-bottom: 10px;
`;

const Container = styled.div`
  padding-top: 20px;
  border-top: solid rgb(206, 206, 206) 1px;
  text-align: center;
`;

const HeaderContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const HeaderText = styled.h2`
  font-size: 40px;
  font-weight: 600;
  line-height: 1.24;
  color: #696969;
  z-index: 0;
  margin: 0;
`;

export default ProfileCard;
