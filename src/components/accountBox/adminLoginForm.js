import React, { useContext, useState } from "react";
import {
  BoldLink,
  BoxContainer,
  FormContainer,
  Input,
  MutedLink,
  SubmitButton,
} from "./common";
import { Marginer } from "../marginer";
import { AccountContext } from "./accountContext";
import { useHistory } from "react-router-dom";
import { UserContext } from "../../userContext";
import { CircularProgress } from "@material-ui/core";
import { useSnackbar } from "notistack";

export function AdminLoginForm() {
  const { switchToSignup } = useContext(AccountContext);
  const { setUser, setLoggedIn } = useContext(UserContext);
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { enqueueSnackbar } = useSnackbar();

  const history = useHistory();

  const handleChange = (e) => {
    if (e.target.name === "email") setEmail(e.target.value);
    if (e.target.name === "password") setPassword(e.target.value);
  };

  const handleLogin = (e) => {
    e.preventDefault();
    setLoading(true);

    fetch("https://switch-online-store.onrender.com/admin/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.isAdmin) {
          localStorage.setItem("accessToken", result.accessToken);
          localStorage.setItem("isAdmin", true);
          localStorage.setItem("isLoggedIn", true);
          setUser({
            accessToken: result.accessToken,
            isAdmin: result.isAdmin,
          });
          enqueueSnackbar("Logged in successfully", { variant: "success" });
          setLoading(false);
          setLoggedIn(true);
          history.push("/products");
        } else {
          enqueueSnackbar("This user is not an admin", { variant: "warning" });
          setLoading(false);
          setPassword("");
        }

        if (
          result.error === "No admin registered with that email address" ||
          result.error === "Incorrect password"
        ) {
          enqueueSnackbar("Invalid credentials, please try again", {
            variant: "error",
          });
          setPassword("");
          setLoading(false);
        }
      });
  };

  return (
    <BoxContainer>
      <FormContainer id="login-form">
        <Input
          type="email"
          name="email"
          placeholder="Email"
          value={email}
          onChange={(e) => handleChange(e)}
        />
        <Input
          type="password"
          name="password"
          placeholder="Password"
          value={password}
          onChange={(e) => handleChange(e)}
        />
      </FormContainer>
      <Marginer direction="vertical" margin="1.6em" />
      {loading ? (
        <CircularProgress color="secondary" />
      ) : (
        <SubmitButton
          type="submit"
          onClick={(e) => handleLogin(e)}
          form="login-form"
        >
          Sign In
        </SubmitButton>
      )}
      <Marginer direction="vertical" margin="2em" />
      <span>
        <MutedLink href="#" onClick={switchToSignup}>
          Don't have an account?
        </MutedLink>
        <BoldLink href="#" onClick={switchToSignup}>
          Sign up
        </BoldLink>
      </span>
      <Marginer direction="vertical" margin="1em" />
    </BoxContainer>
  );
}
