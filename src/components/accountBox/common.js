import styled from "styled-components";

export const BoxContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 10px;
  padding: 10px;
`;

export const FormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  box-shadow: 0px 0px 2.5px rgba(15, 15, 15, 0.19);
`;

export const MutedLink = styled.a`
  font-size: 16px;
  color: #696969;
  font-weight: 500;
  text-decoration: none;
  margin-bottom: 10px;
`;

export const BoldLink = styled.a`
  font-size: 16px;
  color: #00c3e3;
  font-weight: 500;
  text-decoration: none;
  margin: 0 4px;
`;

export const Input = styled.input`
  width: 100%;
  height: 60px;
  outline: none;
  border: 1px solid rgba(200, 200, 200, 0.7);
  padding: 0px 10px;
  border-bottom: 1.4px solid transparent;
  transition: all 200ms ease-in-out;
  font-size: 20px;

  &::placeholder {
    color: rgba(0, 0, 0, 0.6);
  }

  &:not(:last-of-type) {
    border-bottom: 1.5px solid rgba(200, 200, 200, 0.4);
  }

  &:focus {
    outline: none;
    border: 2px solid #00c3e3;
  }
`;

export const SubmitButton = styled.button`
  width: 30%;
  padding: 11px 10%;
  color: #fff;
  font-size: 15px;
  font-weight: 600;
  border: none;
  border-radius: 100px 100px 100px 100px;
  cursor: pointer;
  transition: all, 240ms ease-in-out;
  background-color: #e60012;

  &:hover {
    filter: brightness(1.5);
  }
`;
