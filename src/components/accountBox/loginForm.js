import React, { useContext, useState } from "react";
import {
  BoldLink,
  BoxContainer,
  FormContainer,
  Input,
  MutedLink,
  SubmitButton,
} from "./common";
import { Marginer } from "../marginer";
import { AccountContext } from "./accountContext";
import { useHistory } from "react-router-dom";
import { UserContext } from "../../userContext";
import { CircularProgress } from "@material-ui/core";
import { useSnackbar } from "notistack";

export function LoginForm() {
  const { setLoggedIn, setUser } = useContext(UserContext);
  const { switchToSignup, switchToAdminSignin } = useContext(AccountContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const history = useHistory();

  const handleChange = (e) => {
    if (e.target.name === "email") setEmail(e.target.value);
    if (e.target.name === "password") setPassword(e.target.value);
  };

  const handleLogin = (e) => {
    e.preventDefault();
    setLoading(true);

    fetch("https://switch-online-store.onrender.com/users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.token) {
          if (result.user.isAdmin === true) {
            enqueueSnackbar(
              "This user is an admin. Please log in through the admin login form below",
              { variant: "warning" }
            );
            setPassword("");
            setLoading(false);
          } else {
            setUser({ accessToken: result.token, isAdmin: null });
            localStorage.setItem("accessToken", result.token);
            localStorage.setItem("isAdmin", null);
            localStorage.setItem("isLoggedIn", true);
            setLoading(false);
            enqueueSnackbar("Logged in successfully", { variant: "success" });
            setLoggedIn(true);
            history.push("/");
          }
        }

        if (result.error) {
          enqueueSnackbar("Invalid credentials, try again", {
            variant: "error",
          });
          setPassword("");
          setLoading(false);
        }
      });
  };

  return (
    <BoxContainer>
      <FormContainer id="login-form">
        <Input
          type="email"
          name="email"
          placeholder="Email"
          value={email}
          onChange={(e) => handleChange(e)}
        />
        <Input
          type="password"
          name="password"
          placeholder="Password"
          value={password}
          onChange={(e) => handleChange(e)}
        />
      </FormContainer>
      <Marginer direction="vertical" margin="1.6em" />
      {loading ? (
        <CircularProgress color="secondary" />
      ) : (
        <SubmitButton
          type="submit"
          onClick={(e) => handleLogin(e)}
          form="login-form"
        >
          Sign In
        </SubmitButton>
      )}
      <Marginer direction="vertical" margin="2em" />
      <span>
        <MutedLink href="#" onClick={switchToSignup}>
          Don't have an account?
        </MutedLink>
        <BoldLink href="#" onClick={switchToSignup}>
          Sign up
        </BoldLink>
      </span>
      <BoldLink href="#" onClick={switchToAdminSignin}>
        Or sign in as an admin
      </BoldLink>
      <Marginer direction="vertical" margin="1em" />
    </BoxContainer>
  );
}
