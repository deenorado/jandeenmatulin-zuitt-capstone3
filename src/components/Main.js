import React from "react";
import styled from "styled-components";
import { Route, Switch } from "react-router-dom";
import Home from "../pages/Home";
import Products from "../pages/Products";
import Login from "../pages/Login";
import Orders from "../pages/Orders";
import Profile from "../pages/Profile";

const Main = () => {
  return (
    <MainWrapper>
      <Switch>
        <Route exact path={"/"} component={Home} />
        <Route exact path={"/products"} component={Products} />
        <Route exact path={"/login"} component={Login} />
        <Route exact path={"/orders"} component={Orders} />
        <Route exact path={"/profile"} component={Profile} />
      </Switch>
    </MainWrapper>
  );
};

const MainWrapper = styled.main`
  max-width: ${({ theme }) => theme.widths.content};
  margin: 0 auto;
  padding: 4rem;
  display: flex;
  justify-content: center;
`;

export default Main;
