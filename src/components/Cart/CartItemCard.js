import React, { useState } from "react";
import styled from "styled-components";
import { FaMinus, FaPlus } from "react-icons/fa";
import Button from "../elements/Button";

const CardItemCard = ({ id, title, price, quantity }) => {
  const product = { id, title, price, quantity };

  const formatTitle = () => {
    return product.title.length <= 30
      ? product.title
      : product.title.substr(0, 30) + "...";
  };

  const [productQuantity, setProductQuantity] = useState(quantity);

  const removeFromCart = () => {
    setProductQuantity(productQuantity - 1);

    fetch("https://switch-online-store.onrender.com/users/checkout", {
      method: "POST",
      headers: {
        authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: product.title,
        quantity: -1,
      }),
    })
      .then((res) => res.json)
      .then((result) => {
        // setCart(...cart, (cart.products[indexOfProduct].quantity -= 1));
      });
  };

  const addToCart = () => {
    setProductQuantity(productQuantity + 1);

    fetch("https://switch-online-store.onrender.com/users/checkout", {
      method: "POST",
      headers: {
        authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: product.title,
        quantity: 1,
      }),
    })
      .then((res) => res.json)
      .then((result) => {
        // setCart(...cart, (cart.products[indexOfProduct].quantity += 1));
      });
  };

  const priceOfProduct = () => {
    return parseFloat(price.toFixed(2)).toLocaleString("en");
  };

  return (
    <CartItemCardWrapper>
      <Details>
        <Title>{formatTitle()}</Title>
        <div>₱ {priceOfProduct()}</div>
        <AmountChanger>
          {productQuantity !== 0 && (
            <Button
              onClick={() => removeFromCart()}
              content={<FaMinus />}
              color="grey"
              animation="color"
            ></Button>
          )}
          <div>{productQuantity}</div>
          <Button
            onClick={() => addToCart()}
            content={<FaPlus />}
            color="grey"
            animation="color"
          ></Button>
        </AmountChanger>
      </Details>
    </CartItemCardWrapper>
  );
};

const CartItemCardWrapper = styled.div`
  display: flex;
`;

const Details = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  font-size: 2rem;
`;

const Title = styled.div`
  height: 3rem;
  font-weight: bold;
  overflow: hidden;
`;

const AmountChanger = styled.div`
  display: flex;
  align-items: center;
  gap: 3rem;
`;

export default CardItemCard;
