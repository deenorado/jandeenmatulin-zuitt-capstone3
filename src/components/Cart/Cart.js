import React, { useContext, useEffect, useState } from "react";
import styled, { css } from "styled-components";
import { v4 as uuidv4 } from "uuid";
import Button from "../elements/Button";
import CardItemCard from "./CartItemCard";
import { UserContext } from "../../userContext";

const Cart = () => {
  const [cartItems, setCartItems] = useState([]);
  const { handleCartClose, isCartOpen } = useContext(UserContext);
  const [totalAmount, setTotalAmount] = useState(0);

  useEffect(() => {
    localStorage.getItem("accessToken") &&
      fetch("https://switch-online-store.onrender.com/users/view-orders", {
        headers: {
          authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        },
      })
        .then((res) => res.json())
        .then((result) => {
          setTotalAmount(result.totalAmount);

          setCartItems([
            result.products.map((cartItem) => (
              <CardItemCard
                key={uuidv4()}
                id={cartItem._id}
                title={cartItem.name}
                price={cartItem.price}
                quantity={cartItem.quantity}
              ></CardItemCard>
            )),
          ]);
        });
  }, [isCartOpen]);

  return (
    <React.Fragment>
      <CartWrapper isOpen={isCartOpen}>
        <Title>Your shopping cart</Title>
        <Products>{cartItems}</Products>
        <Total>
          Total: ₱{parseFloat(totalAmount.toFixed(2)).toLocaleString("en")}
        </Total>
        <Button
          content="Checkout"
          size="wide"
          color="primary"
          animation="color"
        />
        <Button
          onClick={() => handleCartClose()}
          content="Close"
          size="wide"
          color="red"
          animation="color"
        />
      </CartWrapper>
      <Overlay onClick={() => handleCartClose()} isOpen={isCartOpen} />
    </React.Fragment>
  );
};

const CartWrapper = styled.div`
  position: fixed;
  z-index: 1;
  top: 0;
  right: -110%;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 4rem;
  width: 59rem;
  height: 100%;
  padding: 6rem;
  background-color: #fff;
  font-size: 3rem;
  transition: right 0.6s ease-in-out;

  ${({ isOpen }) =>
    isOpen &&
    css`
      right: 0;
    `}

  @media (max-width: 480px) {
    width: 100%;
  }
`;

const Title = styled.div`
  margin-bottom: 2rem;
  font-size: 4rem;
  font-weight: bold;
`;

const Products = styled.div`
  display: flex;
  flex-direction: column;
  gap: 3rem;
  width: 100%;
  overflow: auto;
`;

const Total = styled.div`
  font-weight: bold;
`;

const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: -100%;
  width: 100%;
  height: 100%;
  background-color: black;
  opacity: 0.6;
  transition: left 0.6s ease-in-out;

  ${({ isOpen }) =>
    isOpen &&
    css`
      left: 0;
    `}
`;

export default Cart;
