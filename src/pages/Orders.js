import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { v4 as uuidv4 } from "uuid";
import OrderCard from "./Orders/OrderCard";

const Orders = () => {
  const [orders, setOrders] = useState([]);

  useEffect(async () => {
    const controller = new window.AbortController();

    await fetch(
      "https://switch-online-store.onrender.com/admin/all-orders",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        },
      }
    )
      .then((response) => response.json())
      .then((result) => {
        result.error ? setOrders([]) : setOrders(result);
      });

    return () => controller.abort();
  }, []);

  return (
    <React.Fragment>
      <ProductsWrapper>
        {orders !== [] ? (
          orders.map((order) => {
            return (
              <OrderCard
                key={uuidv4()}
                title={order.userId}
                price={order.totalAmount}
                products={order.products}
                id={order._id}
              />
            );
          })
        ) : (
          <div></div>
        )}
      </ProductsWrapper>
    </React.Fragment>
  );
};

const ProductsWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 4rem;
  margin-top: 4rem;

  @media (max-width: 768px) {
    grid-template-columns: repeat(2, minmax(28rem, 36rem));
    justify-content: center;
  }

  @media (max-width: 480px) {
    grid-template-columns: repeat(1, 36rem);
  }

  animation: fadeIn ease 2s;

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
`;

export default Orders;
