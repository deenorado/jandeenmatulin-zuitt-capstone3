import React from "react";
import styled from "styled-components";
import { AccountBox } from "../components/accountBox";

const AppContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Form = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 100px;
  width: 125rem;

  animation: fadeIn ease 2s;

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @media (max-width: 1024px) {
    align-items: center;
  }
`;

function Login() {
  return (
    <AppContainer>
      <Form>
        <AccountBox />
      </Form>
    </AppContainer>
  );
}

export default Login;
