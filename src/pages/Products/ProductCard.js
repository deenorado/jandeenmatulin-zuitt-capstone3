import React, { useState } from "react";
import styled from "styled-components";
import Button from "../../components/elements/Button";
import { CircularProgress } from "@material-ui/core";
import AlertDialogUpdate from "../../components/accountBox/dialogUpdate";
import MouseOverPopover from "../../components/singleProduct/SingleProduct";

const ProductCard = ({ title, price, description, id }) => {
  const product = { title, price, description, id };

  const [loading, setLoading] = useState(false);

  const addToCart = () => {
    setLoading(true);
    fetch("https://switch-online-store.onrender.com/users/checkout", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
      body: JSON.stringify({
        name: product.title,
        quantity: 1,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        setLoading(false);
      });
  };

  const handleDeleteProduct = () => {
    fetch(`https://switch-online-store.onrender.com/admin/archive/${id}`, {
      method: "POST",
      headers: {
        authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json)
      .then((result) => {
        window.location.reload();
      });
  };

  const productButtons = () => {
    if (
      localStorage.getItem("isAdmin") === "null" &&
      localStorage.getItem("isLoggedIn") === "true"
    ) {
      return loading ? (
        <CircularProgress color="secondary" />
      ) : (
        <Button
          onClick={() => addToCart(product)}
          content="Add to cart"
          size="wide"
          color="red"
          animation="color"
        />
      );
    }

    if (
      localStorage.getItem("isAdmin") === "true" &&
      localStorage.getItem("isLoggedIn") === "true"
    ) {
      return (
        <React.Fragment>
          <AlertDialogUpdate id={id} />
          <Button
            onClick={handleDeleteProduct}
            content="Delete"
            size="wide"
            color="red"
            animation="color"
          />
        </React.Fragment>
      );
    }
  };

  return (
    <ProductCardWrapper>
      <Details>
        <Info>
          <MouseOverPopover
            title={product.title}
            id={product.id}
            price={product.price}
            description={product.description}
          />
          <div>₱ {parseFloat(price.toFixed(2)).toLocaleString("en")}</div>
        </Info>
        {productButtons()}
      </Details>
    </ProductCardWrapper>
  );
};

const ProductCardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid ${({ theme }) => theme.colors.grey.main};
  border-radius: 10px;
  background-color: #fff;
  font-size: 2rem;
`;

const Details = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  gap: 2rem;
  height: 100%;
  padding: 2rem;
  border-top: 1px solid ${({ theme }) => theme.colors.grey.main};
`;

const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  gap: 1rem;
  height: 100%;
`;

export default ProductCard;
