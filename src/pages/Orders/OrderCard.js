import { Card, CardContent, makeStyles, Typography } from "@material-ui/core";
import { red } from "@material-ui/core/colors";
import React from "react";
import styled from "styled-components";
import Button from "../../components/elements/Button";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: "56.25%",
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

const OrderCard = ({ title, price, products, id }) => {
  const classes = useStyles();
  const order = { title, price, products, id };

  const handleDeleteProduct = () => {
    fetch(
      `https://switch-online-store.onrender.com/admin/delete-order/${id}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        },
      }
    )
      .then((res) => res.json())
      .then((result) => {
        window.location.reload();
      });
  };

  const orders = order.products.map((prod) => {
    return (
      <li>
        <Typography>{prod.name}</Typography>
        <Typography color="secondary">{`${prod.quantity} x`}</Typography>
      </li>
    );
  });

  return (
    <Card className={classes.root} elevation={3}>
      <h2 style={{ padding: "1.5rem" }}>{`Order ID: ${id.slice(0, 18)}`}</h2>
      <p style={{ padding: "1.5rem", fontSize: "2rem" }}>{`₱ ${parseFloat(
        price.toFixed(2)
      ).toLocaleString("en")}`}</p>
      <OrderCardWrapper>
        <Details>
          <Info>
            <CardContent>
              <ul style={{ listStyle: "none" }}>{orders}</ul>
            </CardContent>
          </Info>
        </Details>
      </OrderCardWrapper>
      <p style={{ padding: "1.5rem" }}>
        <strong>Ordered by: </strong>
        {title}
      </p>
      <Button
        onClick={() => handleDeleteProduct()}
        content="Delete"
        size="wide"
        color="red"
        animation="color"
      />
    </Card>
  );
};

const Info = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1rem;
  height: 100%;
`;

const OrderCardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid ${({ theme }) => theme.colors.grey.main};
  border-radius: 10px;
  background-color: #fff;
  font-size: 2rem;
`;

const Details = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  gap: 2rem;
  height: 100%;
  padding: 2rem;
`;

export default OrderCard;
