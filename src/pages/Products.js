import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { v4 as uuidv4 } from "uuid";
import ProductCard from "./Products/ProductCard";
import AlertDialogSlide from "../components/accountBox/dialogCreate";

const Products = () => {
  const [products, setProducts] = useState([]);

  useEffect(async () => {
    const controller = new window.AbortController();

    const response = await fetch(
      "https://switch-online-store.onrender.com/products"
    );

    let data = await response.json();

    setProducts(data);

    return () => controller.abort();
  }, []);

  const productCards = products.map((product) => {
    return (
      <ProductCard
        key={uuidv4()}
        title={product.name}
        price={product.price}
        id={product._id}
        description={product.description}
      />
    );
  });

  return (
    <React.Fragment>
      <ProductsWrapper>
        {productCards}
        {localStorage.getItem("isAdmin") === "true" && <AlertDialogSlide />}
      </ProductsWrapper>
    </React.Fragment>
  );
};

const ProductsWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 4rem;
  margin-top: 4rem;

  @media (max-width: 768px) {
    grid-template-columns: repeat(2, minmax(28rem, 36rem));
    justify-content: center;
  }

  @media (max-width: 480px) {
    grid-template-columns: repeat(1, 36rem);
  }

  animation: fadeIn ease 2s;

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
`;

export default Products;
