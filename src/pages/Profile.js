import React from "react";
import { useState, useEffect } from "react";
import styled from "styled-components";
import ProfileCard from "../components/accountBox/profileCard";

const AppContainer = styled.div`
  width: 20%;
  height: 20%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Form = styled.div`
  display: flex;
  flex-direction: column;
  width: 125rem;

  animation: fadeIn ease 2s;

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @media (max-width: 1024px) {
    align-items: center;
  }
`;

const Profile = () => {
  const [user, setUser] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  const getUser = async () => {
    return await fetch(
      "https://switch-online-store.onrender.com/users/profile",
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        },
      }
    )
      .then((res) => res.json())
      .then((result) => result);
  };

  useEffect(async () => {
    await getUser().then((result) => {
      setUser(result);
      setIsLoading(false);
    });
  }, []);

  if (isLoading) return <div></div>;
  else {
    return (
      <AppContainer>
        <Form>
          <ProfileCard
            email={user[0].email}
            id={user[0]._id}
            fullName={user[0].fullName}
            about={user[0].about}
          />
        </Form>
      </AppContainer>
    );
  }
};

export default Profile;
